function thrust = maxThrust(thrust)
% maxThrust guarantees that the value stays between [-100,100].
% In case the value is outside the conditions below, and the same value
% that was sent, is returned again.
if thrust > 100
    thrust = 100;
elseif thrust < -100
    thrust = -100;
end

end