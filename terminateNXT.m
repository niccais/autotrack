function terminateNXT(handle_NXT,mV,mH)

if nargin == 3
    % stopp motorer
    mV.Stop;
    mH.Stop;
end

%Steng sensor
CloseSensor(SENSOR_1);

% Clear MEX-file to release joystick
clear joymex2

% Close NXT connection.
COM_CloseNXT(handle_NXT);

end