function [mV, mH] = engineHandler(Power, Turn, mH, mV)
% engineHandler receives power straight ahead and turn power. Additionally
% the NXTMotor objects are received so they can be updated and sent to the
% NXT.
%% Round Power and Turn up to nearest integer
Turn        = round(Turn);
Power       = round(Power);
%% Verify power amount
% Check that Power sent to motor is between [-100,100] for each motor
% seperatly
mH.Power = maxThrust(Power - Turn);
mV.Power = maxThrust(Power + Turn);
%% Send new values to NXT
mH.SendToNXT();
mV.SendToNXT();

end