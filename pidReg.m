function out = pidReg(Kp,Kd,Ki,e,area,time)

global i;

out = Kp * e(i) + Ki * intData(area(i-1),e,time) + Kd * differ(e,time);

end