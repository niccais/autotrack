
handle_NXT = handshake;

OpenLight(SENSOR_1, 'ACTIVE');      % Light sensor

[light,area, Time,...
    diff, filter, dev,...
    pid, devUnfilt]                     = deal(zeros(1,1));
time                                    = uint64(zeros(1,1));

sLight                                  = GetLight(SENSOR_1);

global i;
global running;
[i,running]         = deal(1);

Kp = 1;
Kd = 1/2;
Ki = 1/10;

set(0,'DefaultFigureUnits','normalized');
figure('Position',[0.1,0.1,0.8,0.7],'CloseRequestFcn',@onClose);
%figure('CloseRequestFcn',@onClose);
h1 = subplot(2,2,1);
p1 = plot([0],[0],'-r'); hold on
p1b = plot([0],[0],'-k');
%set(gca,'xlim',[0 15],'ylim',[-300 300]);
legend(p1b,'raw');

h2 = subplot(2,2,3);
p2 = plot([0],[0],'-g');
%set(gca,'xlim',[0 15],'ylim',[-300 300]); axis square
legend('area');

h3 = subplot(2,2,2);
p3 = plot([0],[0],'-b');
%set(gca,'xlim',[0 15],'ylim',[-300 300]);
legend('differ');

h4 = subplot(2,2,4);
p4 = plot([0],[0],'-m');


while running
   
    light(i)    = GetLight(SENSOR_1);
    time(i)     = tic;
    Time(i)     = toc(time(1));
    if i == 5
        sLight  = GetLight(SENSOR_1);
    end
    
    filter(i) = filterData(filter,light,20);
    devUnfilt(i)= light(i) - sLight;
    dev(i)      = filter(i) - sLight;
    
    
    if i > 1
        area(i)     = intData(area(i-1),dev,time);
        diff(i)     = differ(dev,time);
        pid(i) = pidReg(Kp,Kd,Ki,dev,area,time)/30;
    end
    
    set(p1, 'Xdata', Time, 'Ydata', dev);
    set(p1b, 'Xdata', Time, 'Ydata', light-sLight);
    set(p2, 'Xdata', Time, 'Ydata', area);
    set(p3, 'Xdata', Time, 'Ydata', diff);
    set(p4, 'Xdata', Time, 'Ydata', pid);
    set([h1 h3], 'xlim', [Time(i)-18 Time(i)+2]);
    legend(p1,['filter ' num2str(dev(i))]);
    
    drawnow
    
    i = i+1;
end

terminateNXT(handle_NXT);

subplot(2,2,1);
plot(Time(30:90),dev(30:90),'-r'); hold on
plot(Time,light-sLight,'-k');
legend('filter','raw');

subplot(2,2,3:4);
plot(Time,area,'-g');
legend('area');

subplot(2,2,2);
plot(Time,diff,'-b');
legend('differ');