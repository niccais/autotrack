function value = filterData(old,data,limit)

global i;
if i >= limit
    value = 0;
    if data(i) < old(i-1)-3 || data(i) > old(i-1)+3
        for j = 1:limit
        value = value + 1/limit * data(i+1-j);
        end
    else
        value = old(i-1);
    end
else
    value = data(i);
end

end