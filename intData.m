function integr = intData(old, value, time)
% intData calculates the integral of the current value relative to the
% elapsed time since the last loop. Old is a double and represents the
% previous result. value is an array of data and time is an array of tic's.
global i;   % Global indexing value to track current location in loop

% First run only calculates initial time multiplied with the initial value.
% All other runs sum this calculation with the previous one.
if i > 1
    % toc(time(i-1)) fetches the time tic was called "one loop ago".
    % resulting in a representation of the time it takes for one loop.
    integr = old + value(i)*(toc(time(i-1)));
else
    integr = value(i)*toc(time(i));
end

end