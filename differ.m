function d = differ(value,time)
% differ requires a value array and a time array. The value can be a
% double, but the time needs to be an array of tic values.
global i; % Global indexing value to track current location in loop

if i > 1
    % after second loop, the difference in value relative to the difference
    % in time is calculated.
    d = (value(i)-value(i-1))/(toc(time(i-1)));
else
    % assumes there is no difference in the initial loop.
    d = 0;
end

end