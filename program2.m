%% Automatic Tracking
%
% Gruppe 1326
%
% Program for NXT to automatically trace path on track.


%% Initialize process
handle_NXT  = handshake();          % Establish connection to NXT
mV          = initMotor('C');       % Left motor
mH          = initMotor('B');       % Right motor
OpenLight(SENSOR_1, 'ACTIVE');      % Light sensor
OpenSound(SENSOR_2,'DB');           % Sound sensor

%% Variables
%%% Arrays
[d, light, area, Deriverte,...
    filter, deviation,...
    tElapsed, correction]   = deal(zeros(1,1));
% time saves tic stamps, which need to be saved as 64-bit unsigned int's
time                        = uint64(zeros(1,1));

%%% Constants
global running;             % running needs to be updated within functions
global i;                   % i is used everywhere.
dataLimit                   = 3;   % How detailed should the filter be
power                       = 20;

%%% Start values
[running, i, n]             = deal(1);
outOfBounds                 = 0;

%%% PID regulator constants
Kp      = 4;
Kd      = 1/4;
Ki      = 1/2;

%% Plots
% Set default units for when applying figure position.
set(0,'DefaultFigureUnits','normalized');
% Create figure, define position and size and add a CloseRequestFcn to stop
% program upon closing the figure.
figure('Position',[0.1,0.1,0.8,0.7],...
    'CloseRequestFcn',@onClose,...
    'name','fig1');
% empty placeholder plot named p1 for updating values during loop.
p1 = plot([0],[0],'-m');

%
%Wait for clap
waitingForSound = 1;
while(waitingForSound)
    waitingForSound = checkSound(GetSound(SENSOR_2));
    if not(waitingForSound)
        running = 1;
    end
end

%% The Loop

while running
    %% Fetch data
    %%% Time
    time(i)             = tic;
    tElapsed(i)         = toc(time(1));         % Total time elapsed
    
    %%% Sensors
    light(i)            = GetLight(SENSOR_1);        % Get new light data
    if i == 3
        n               = 3;    % Because the light changes after red diod 
    end                         % turns on, a new default is needed
    
    %% Calculations
    %%% Light filter
    % Collected light data is filtered to produce a smoother curve.
    filter(i)       = filterData(filter,light,dataLimit);
    %%% Deviation
    % Calculate deviation from initial light scanned.
    deviation(i)    = filter(i)-light(n);
    
    % Due to the following functions using data from previous runs, we need
    % to enrsure that the loop has at least run once before calculating the
    % following values.
    if i > 1
        %%% Integral
        % Integrate the data collected
        area(i)         = intData(area(i-1),deviation,time);
        %%% Engine
        % A PID regulator is used for correcting the deviation. It is
        % devided by 80 so that the value stays within the desired range.
        correction(i)   = (pidReg(Kp, Kd, Ki, deviation, area, time))/20;
        % Now that the heading has been calculated, everything is sent to
        % the motors. The values are sent back for plotting purposes.
        d(i)            = differ(deviation,time);
        [mV, mH]        = engineHandler(power, correction(i), mH, mV);
    end
    %% Update plots
    % Data from the PID regulator is plotted to display the magnitude of
    % needed correction.
    set(p1, 'Xdata', tElapsed, 'Ydata', correction);
    
    %% End of loop
    
    % Check if outside the track.
    if isOutOfBounds(filter(i))
        msgbox('Du kj�rte utenfor banen', 'Game Over');
    end
    
    drawnow
    i = i + 1;
end

close(findobj('type','figure','name','fig1'));

calcScore(sum(abs(area)),tElapsed(i-1));

terminateNXT(handle_NXT, mV, mH);

%subplot(2,2,1);
plot(tElapsed,deviation,'-r'); hold on
plot(tElapsed,d/4,'-b');
plot(tElapsed,area,'-g');
plot(tElapsed,correction,'-m');
plot(tElapsed,(5*deviation+(1)*d+1*area)/20,'-k');

legend('deviation','derived','area','correction','calculated');

% 
% subplot(2,2,3);
% plot(tElapsed,area,'-g');
% legend('area');
% 
% subplot(2,2,4);
% plot(tElapsed,d,'-b');
% legend('derived');
% 
% subplot(2,2,2);
% plot(tElapsed,correction,'-m');
% legend('correction');